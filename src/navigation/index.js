import 'react-native-gesture-handler';
import React, { useState, useEffect } from 'react';
import { StyleSheet } from 'react-native';
import SideMenu from '../components/SideMenu';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import RnBgTask from 'react-native-bg-thread';
import  { BackHandler } from 'react-native';


import {
  Login,
  Home,
  Signup,
  Student,
  Teacher,
  Historial,
  Record,
  Inscription,
  Subjects,
  RecordNotes,
  Update
} from '../screens'

const Drawer = createDrawerNavigator();
const screens = [
  {
    name: "Login",
    component: Login
  },
  {
    name: "Home",
    component: Home
  },
  {
    name: "Signup",
    component: Signup
  },
  {
    name: "Student",
    component: Student
  },
  {
    name: "Historial",
    component: Historial
  },
  {
    name: "Record",
    component: Record
  },
  {
    name: "Inscription",
    component: Inscription
  },
  {
    name: "Teacher",
    component: Teacher
  },
  {
    name: "RecordNotes",
    component: RecordNotes
  },
  {
    name: "Subjects",
    component: Subjects
  },
  {
    name: "Update",
    component: Update
  }
]
export default function Navigation() {
  let n = 1;
  useEffect(() => {
    RnBgTask.runInBackground(() => {    
      // setTimeout(function(){       
      //    BackHandler.exitApp();
      // }, 10000)
    })
  })


  const [loading, setLoading] = useState(true);
  const [initialRoute, setInitialRoute] = useState('Login');
  return (
    // <Provider store={store}>
    <NavigationContainer>
      <Drawer.Navigator
        initialRouteName={initialRoute}

        drawerStyle={styles.drawer}
        drawerContent={(props) => <SideMenu {...props}
        />}
      >
        {screens?.map(({ name, component }) => (
          <Drawer.Screen key={name} name={name} component={component} />
        )
        )}
      </Drawer.Navigator>
    </NavigationContainer>

    // </Provider>

  );
};
const styles = StyleSheet.create({
  drawer: {
    width: '50%',
    backgroundColor: '#fff',
  },
})