import React from 'react';

import {
    StyleSheet,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    ImageBackground,
    Animated,
    SegmentedControlIOSComponent
} from 'react-native';
import { Input, Block, theme } from 'galio-framework';
import Theme from '../constants/Theme'

export default function textInput(props) {
    const inputStyles = [
        styles.input,
        props.shadow && styles.shadow,
        props.success && styles.success,
        props.error && styles.error,
        //{height: props.height?200:40},
        
      ];
    return (
        <Input
            placeholder={props.pleaceHolder}
            style={inputStyles}            
            placeholderTextColor={Theme.COLORS.GREY}
            />
    )
}
const styles=StyleSheet.create({    
      input: {
        backgroundColor: '#EFEFEF',
        width: '90%',       
        borderRadius: 10,
    },
    
})