import React, { useState } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import Theme from '../constants/Theme';
import IconButton from './IconButton';


export default function HeaderHome({
    title,
    color,
    backgroundColor,
    navigation,
}) {

   
    return (
        <View style={styles.header}>
            {/* <NotificationIndicator showIndicator={hasNotifications}> */}
            <IconButton
                name="menu"
                family="MaterialIcons"
                size={30}
                iconColor={color}
                color={backgroundColor}
                onPress={() => {
                    navigation.openDrawer();
                }}
            />
            {/* </NotificationIndicator> */}
            <View style={styles.center}>
                <Text style={styles.title}>{title}</Text>
            </View>
            <IconButton
                name="notifications"
                family="MaterialIcons"
                size={30}
                iconColor={color}
                color={backgroundColor}
                onPress={() => {
                    navigation.navigate("Notifications")
                }}
            />

        </View>
    );
}

const styles = StyleSheet.create({
    header: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        height:'8%',
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor:Theme.COLORS.PRIMARY,
        elevation:5,
    },
    title: {
        //paddingRight: 130,
        fontFamily: 'Redressed',
        fontSize: 19,
        color: Theme.COLORS.BACKGROUNDCOLOR,
        textAlign:'right',
        //borderWidth:1,
    },
    subtitle: {
        fontSize: 10,
    },
    emptyIcon: {
        width: 40,
        height: 40,
    },
    center: {
        alignItems: 'center',
        //borderWidth:1,
        //width: '80%',
    },
    input: {
        width: '95%',
        height: 40,
    },
    logo: {
        width: 40,
        height: 40,
    },
});
