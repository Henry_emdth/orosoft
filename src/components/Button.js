import React from 'react';
import { StyleSheet, View, TouchableHighlight } from 'react-native';
import { Button as ButtonGalio } from 'galio-framework'
import Theme from '../constants/Theme'
export default function Button(props) {
  
  const buttonStyles = [
    props.small && styles.smallButton,
    props.medium && styles.mediumButton,
    !(props.small || props.medium) && props.large && styles.largeButton,
    //props.color && {backgroundColor: colorStyle},
    // props.shadow && styles.shadow,
    styles.button,props.style
  ];
  return (
    <ButtonGalio
      style={buttonStyles}
      round
      opacity={0.2}
      color={props.color?props.color:(props.disabled?Theme.COLORS.GREY:Theme.COLORS.PRIMARY)}
      disabled={props.disabled}
      onPress={()=>props.press()
      }
    >
      {props.text}
    </ButtonGalio>
  )

}


const styles = StyleSheet.create({
  smallButton: {
    width: 100,
    height: 35,
    
  },
  mediumButton: {
    width: 250,
    height: 35,
  },
  largeButton: {
    width: '90%',
    height: 35,
  },
  button: {
    marginHorizontal: '5%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  shadow: {
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 4 },
    shadowRadius: 4,
    shadowOpacity: 0.1,
    elevation: 2,
  },

  text: {
    fontSize: 16,
    fontWeight: '700',
  },
});


