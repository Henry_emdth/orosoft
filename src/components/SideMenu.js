import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, Image, Linking } from 'react-native';
import { Icon, Button as ButtonGalio } from 'galio-framework';
import auth from '@react-native-firebase/auth';
import Theme from '../constants/Theme'
import { ScrollView } from 'react-native-gesture-handler';
import firestore from '@react-native-firebase/firestore';

//import { useDispatch } from 'react-redux'
//import { fetchUserContact, fetchedUserProfile } from '../store/actions/user';


const imageVoid = require('../assets/profile.png');

export default function SideMenu({ navigation }) {
  const [type, setType] = useState('')
  const [loged, setLoged] = useState();
  useEffect(() => {    
        auth().onAuthStateChanged(function (user) {
            if (user) {
                setLoged(user.uid)                
                setType(user.displayName);
            }
        });   
  }, [loged]);
  //console.log(loged,'----')
 // console.log(type,']]]]]]]]]]')
  const student =[
    // {
    //   name: 'Inicio',
    //   navigateTo: 'Home',
    // },
    // {
    //   name: 'Historial Academico',
    //   navigateTo: 'Home',
    // },
    // {
    //   name: 'Actualizar datos',
    //   navigateTo: 'Home',
    // },
    // {
    //   name: 'Inscripcion de materias',
    //   navigateTo: 'Inscription',
    // },
    // {
    //   name: 'Record Academico',
    //   navigateTo: 'Home',
    // },
    // {
    //   name: 'Docentes',
    //   navigateTo: "Home",
    // },
    // {
    //   name: 'Pensum',
    //   navigateTo: "Home",
    // },
    // {
    //   name: 'Contenido Analitico',
    //   navigateTo: "Home",
    // }


    {
      name: 'Mis datos',
      navigateTo: 'Student',
    },
    {
      name: 'Historial Academico',
      navigateTo: 'Historial',
    },
    {
      name: 'Actualizar datos',
      navigateTo: 'Update',
    },
    {
      name: 'Inscripcion de materias',
      navigateTo: 'Inscription',
    },
    {
      name: 'Mi horario',
      navigateTo: 'Record',
    },
     {
      name: 'Contenido Analitico',
      navigateTo: "Home",
    },
  ]
  const teacher=[
    {
      name: 'Mis datos',
      navigateTo: 'Teacher',
    },
    {
      name: 'Actualizar datos',
      navigateTo: 'Update',
    },
    {
      name: 'Registrar Notas',
      navigateTo: 'RecordNotes',
    },
    {
      name: 'Materias Asignadas',
      navigateTo: 'Subjects',
    },
    {
      name: 'Ver Estudiantes',
      navigateTo: 'Teacher',
    }
  ]  

  const user = {
    photo: null,
    firstName: 'Welib',
    lastName: 'Baja'
  }
const items=(type==='students')?student:teacher;
  return (
    <View style={styles.container}>
      <View style={{ marginTop: '15%' }}>
        <Image
          source={user.photo ? null : imageVoid}
          style={styles.image}
        />
        <Text style={styles.title}>{user.firstName + ' ' + user.lastName}</Text>
      </View>
      <View style={styles.itemsContainer}>
        <ScrollView>
          {items.map(({ name, navigateTo }) => {
            return (
              <ButtonGalio
                key={name}
                color={Theme.COLORS.PRIMARY}
                shadowless
                style={styles.menuItem}
                onPress={() => {
                  name === 'Mis contactos' && dispatch(fetchUserContact())
                  //name === 'Mensajes' && dispatch(fetchedUserProfile(null))
                  navigateTo &&
                    navigation.navigate(navigateTo, { name });
                }}>
                <Text style={styles.text}> {name}</Text>
              </ButtonGalio>
            );
          })}
        </ScrollView>
      </View>
      <View style={{ backgroundColor: Theme.COLORS.PRIMARY, width: '95%' }}>
        <ButtonGalio
          color={Theme.COLORS.PRIMARY}
          shadowless
          style={styles.menuItem}
          onPress={() => {
            //navigation.navigate('Login');
            logOut()
          }}>
          <Text style={styles.text}>Cerrar sesión</Text>
        </ButtonGalio>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    width: '100%',
    height: '100%',
  },
  image: {
    alignSelf: 'center',
    height: 60,
    width: 60,
    borderRadius: 50,
    marginTop: '20%'
  },
  itemsContainer: {
    backgroundColor: Theme.COLORS.PRIMARY,
    width: '100%',
    //height: 284,   
  },
  menuItem: {
    width: '100%',
    height: 40,
    //display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingLeft: '10%',
    //borderStyle: 'solid',
    borderBottomColor: Theme.COLORS.BACKGROUNDCOLOR,
    borderBottomWidth: 1,
    marginVertical: 0,
  },
  text: {
    color: '#fff',
  },
  title: {
    marginTop: 10,
    color: '#fff',
    color: '#000',
    fontSize: 18,
    alignSelf: 'center'
  }
});
