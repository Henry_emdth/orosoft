import React from 'react';
import {View, Text, StyleSheet, TouchableHighlight} from 'react-native';
import {Icon} from 'galio-framework';

export default function IconButton({
  name,
  family,
  size,
  color,
  iconColor,
  style,
  text,
  onPress,
}) {
  const iconStyles = [styles.container, style, {backgroundColor: color}];
  return (
    <View style={iconStyles}>
      <TouchableHighlight>
        <Icon
          name={name}
          family={family}
          color={iconColor}
          size={size}
          onPress={onPress}
          style={styles.noMargin}
        />
      </TouchableHighlight>
      {text && <Text style={[styles.text, {color: iconColor}]}>{text}</Text>}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  text: {
    fontSize: 14,
  },
  noMargin: {
    margin: 0,
    width:'100%',
  },
});
