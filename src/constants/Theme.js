export default {
    COLORS: {
      //PRIMARY: '#304FFE',
      PRIMARY: '#049FFF',  
      SECONDARY:'#BEDEE8',    
      BACKGROUNDCOLOR:'#E5E5E5',
      WHITE: '#FFFFFF',
      BLACK: '#151522',
      DARKGREY: '#30332E',
      LIGHTGREY: '#EDF1F9',
      GREY: '#B5BBC9',
      ERROR: '#FF647C', 
      SUCCESS: '#00C48C',    
      WARNING: '#FB6340',
      GRADIENT_START: '#343D63',
      GRADIENT_END: '#3D639D',
      BUTTON:'#53D769',
    },
    FONT: {
      FAMILY: 'Redressed',
      VERYSMALL: 10,
      SMALL:14,
      MEDIUM:18,
      BIG:20,
    },
    CLASSES: {
      CENTER: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
      },
    },
    FONT: {
      FAMILY: 'Redressed',
      VERYSMALL: 10,
      SMALL:14,
      MEDIUM:18,
      BIG:20,

    },

  };
  