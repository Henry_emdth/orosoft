import Login from './Login'
import Home from './Home'
import Signup from './Signup'
import Update from './Update';
import Student from "./student/Student";
import Historial from "./student/Historial";
import Record from "./student/Record";
import Inscription from "./student/Inscription";
import Teacher from "./teacher/Teacher";
import RecordNotes from "./teacher/RecordNotes";
import Subjects from "./teacher/Subjects";

export {
    Login,
    Home,
    Signup,
    Update,
    Student,
    Historial,
    Record,
    Inscription,

    Teacher,
    RecordNotes,
    Subjects
} 