
import React, { useEffect, useRef, useState, useMemo } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    Image,
    Alert,
    Pressable,
    ScrollView,
    FlatList,
    SafeAreaView
} from 'react-native'
import Theme from '../../constants/Theme'
import Button from '../../components/Button'
import Input from '../../components/Input';
import HeaderHome from '../../components/HeaderHome'
import firestore from '@react-native-firebase/firestore';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Icon, theme } from 'galio-framework';
import auth, { firebase } from '@react-native-firebase/auth';
import * as Animatable from 'react-native-animatable';


export default function Subject({ navigation }) {
    const [loged, setLoged] = useState();
    const [subjects, setSubjects] = useState([]);
    const [data, setData] = useState();
    const [form, setForm] = useState({
        firstName: { value: 'Ivan', required: true, error: true },
        lastName: { value: 'Escalante', required: true, error: true },
        email: { value: 'ivan@test.com', required: true, error: true },
        phone: { value: '76543210', required: true, error: true },
        address: { value: 'Z/Portada C/Ala Nro:1311', required: true, error: true },
        ci: { value: '8871242', required: true, error: true },
    });

    useEffect(() => {
        auth().onAuthStateChanged(function (user) {
            if (user) {
                setLoged(user.uid)
                //console.log(user, '===')
                firestore()
                    .collection('teachers')
                    .doc(loged)
                    .onSnapshot(querySnapshot => {
                        if (querySnapshot.data()) {
                            setData(querySnapshot.data()); getData()
                        }
                    })


            }
        })


    }, [loged]);

    const getData = () => {

        firestore().collection("subjects")
            .where("idTeacher", "==", 'Lb45yLtNICWFGQKeEdUIRi93PXz2')
            .get()
            .then(querySnapshot => {
                querySnapshot.forEach(doc => {
                    console.log('MedData' )
                    setSubjects([doc.data()])
                    //medName = doc.data().DSCRD
                })
            })
    }

    console.log(subjects, '--+++data')
    const materias = [
        {
            "Sigla": "INF111",
            "Nombre": "Introduccion a la informatica",
            "Cantidad": "52",
            "Auxiliar": "Aux: Marco Efe",
        }, {
            "Sigla": "INF121",
            "Nombre": "Algoritmos y programacion",
            "Cantidad": "52",
            "Auxiliar": "Aux: Marco Efe",
        }, {
            "Sigla": "INF151",
            "Nombre": "Sistemas operativos",
            "Cantidad": "23",
            "Auxiliar": "Aux: Marco Efe",
        }, {
            "Sigla": "LAB131",
            "Nombre": "Laboratorio de estrutura de daots",
            "Cantidad": "52",
            "Auxiliar": "Aux: Marco Efe",
        }, {
            "Sigla": "INF111",
            "Nombre": "Introduccion a la informatica",
            "Cantidad": "52",
            "Auxiliar": "Aux: Marco Efe",
        }, {
            "Sigla": "INF111",
            "Nombre": "Introduccion a la informatica",
            "Cantidad": "52",
            "Auxiliar": "Aux: Marco Efe",
        }, {
            "Sigla": "INF111",
            "Nombre": "Introduccion a la informatica",
            "Cantidad": "52",
            "Auxiliar": "Aux: Marco Efe",
        }
    ]
    return (
        <>
            <HeaderHome
                navigation={navigation}
                color={Theme.COLORS.BACKGROUNDCOLOR}
                title={"MATERIAS ASIGNADAS"}
            />
            <View style={styles.container}>
                <View style={styles.profile}>
                    <Image
                        style={styles.stretch}
                        source={require('../../assets/profile.png')}
                    //source={{ uri: 'https://reactnative.dev/img/tiny_logo.png' }}
                    />
                    <View style={styles.textInfo}>
                        <Text style={{ color: Theme.COLORS.LIGHTGREY }}>
                            Nombre: {data?.firstName} {data?.lastName}{'\n'}
                            C.I.: {data?.ci} {'\n'}
                            Correo Electronico: {data?.email}
                        </Text>
                    </View>
                </View>
                <View style={{ width: '90%' }}>
                    <FlatList
                        data={subjects}
                        renderItem={
                            ({ item }) =>
                                <Animatable.View style={styles.containerSubjects} animation={'fadeInLeft'}
                                duration={1000}>
                                    <View style={styles.initials}><Text style={{ color: Theme.COLORS.LIGHTGREY }}>{item.initial}</Text></View>
                                    <View style={{ padding: 10, flex: 1 }}>
                                        <View style={styles.description}><Text style={styles.textDescription}>Nombre: </Text><Text>{item.name}</Text></View>
                                        <View style={styles.description}><Text style={styles.textDescription}>Cantidad de Estudiantes: </Text><Text>{item.quantity}</Text></View>
                                        <View style={styles.description}><Text style={styles.textDescription}>Nombre de Auxiliar: </Text><Text>{item.aux}</Text></View>
                                    </View>
                                </Animatable.View>
                        }
                    //keyExtractor={item => item.id}
                    />
                </View>
            </View>
        </>
    )
}
const styles = StyleSheet.create({
    stretch: {
        width: '20%',
        height: 50,
        marginVertical: 10,
        borderRadius: 200,
    },
    container: {
        backgroundColor: Theme.COLORS.LIGHTGREY,
        flex: 1,
        alignItems: 'center',
    },
    containerSubjects: {
        borderWidth: 1,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        borderBottomRightRadius: 5,
        borderBottomLeftRadius: 5,
        marginVertical: 4,
    },
    description: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    initials: {
        backgroundColor: '#386199',
        padding: 10,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        alignItems: 'center'
    },

    profile: {
        width: '90%',
        marginVertical: 10,
        alignItems: 'center'
    },
    textDescription: {
        fontSize: 18,
        fontStyle: 'italic'
    },
    textInfo: {
        width: '90%',
        padding: 10,
        backgroundColor: Theme.COLORS.WARNING,
        borderRadius: 10,
    },
})
