
import React, { useEffect, useRef, useState, useMemo } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    Image,
    Alert,
    Pressable,
    ScrollView,
    FlatList,
    SafeAreaView
} from 'react-native'
import Theme from '../../constants/Theme'
import Button from '../../components/Button';
import DropDownPicker from 'react-native-dropdown-picker';
import HeaderHome from '../../components/HeaderHome'
import firestore from '@react-native-firebase/firestore';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Icon } from 'galio-framework';
import auth from '@react-native-firebase/auth';
import { sub } from 'react-native-reanimated';


export default function Inscription({ navigation }) {
    const action = () => {
        Alert.alert(
            "DATOS PERSONALES",
            "Nombre: Henry Villalobos",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "OK", onPress: () => console.log("OK Pressed") }
            ],
            { cancelable: false }
        );
    }
    const [form, setForm] = useState({
        initial: { value: '', required: true, error: true },
        name: { value: '', required: true, error: true },
        teacher: { value: '', required: true, error: true },
        idTeacher: { value: '', required: true, error: true },
        idStudent: { value: '', required: true, error: true },
        //ci: { value: '8871242', required: true, error: true },
    });
    const [data, setData] = useState([])
    const [loged, setLoged] = useState()



    console.log(data, '===teacher')
    const postStudent = (data) => {
        firestore()
            .collection('students')
            .doc(data.ci.value)
            .set({
                firstName: data.firstName.value,
                lastName: data.lastName.value,
                email: data.email.value,
                phone: data.phone.value,
                address: data.address.value,
                ci: data.ci.value,
            })
            .then(() => {
                navigation.navigate('Home');
            }
            )
            .catch((error) => {
                console.log(
                    'Something went wrong with added user to firestore: ',
                    error,
                );
            });
    }


    useEffect(() => {
        firestore().collection('teachers').get()
            .then(snapshot => {
                snapshot.forEach(doc => {
                    //console.log(doc.id, '=>', doc.data());
                    setData([...data, { id: doc.id, ...doc.data() }])
                });
            })
            .catch(err => {
                console.log('Error getting documents', err);
            });
        auth().onAuthStateChanged(function (user) {
            if (user) {
                setLoged(user.uid)
            }
        });

    }, []);
    console.log(form, loged, '-------')

    const registry = (subject) => {
        firestore()
            .collection('subjects')
            .add({
                initial:subject.initial.value, 
                name:subject.name.value,
                teacher: subject.teacher.value,
                idTeacher: subject.idTeacher.value,
                idStudent: loged,
                aux:'Aux -- por definir--',
                quantity:'1'
            })
            .then(() => {
                console.log("Registro Exitoso");
                //navigation.navigate('Contacts', { type: 'register' });
            })
            .catch((error) => {
                console.log(
                    'Something went wrong with added user to firestore: ',
                    error,
                );
            });
    }

    return (
        <View style={styles.container}>
            <HeaderHome
                navigation={navigation}
                color={Theme.COLORS.BACKGROUNDCOLOR}
                title={"REGISTRO DE MATERIA"}
            />
            <View style={styles.container}>

                <View style={{ width: '90%', height: '95%', backgroundColor: '#fff', borderRadius: 20, elevation: 7, alignItems: 'center', paddingTop: 20 }}
                >
                    <Image style={{ marginBottom: 10, width: 70, height: 70, borderWidth: 5, borderColor: '#ddd', borderRadius: 200 }}
                        source={require('../../assets/profile.png')} />
                    <Text>Universidad Mayor de San Andres</Text>

                    <View style={{ width: '100%', height: '100%', }}>
                        <View style={styles.inputContainer}>
                            <Text style={styles.textInput}>Nombre</Text>
                            <TextInput
                                style={styles.input}
                                placeholder={'Introduzca nombre'}
                                placeholderTextColor="#888"
                                value={form.name.value}
                                onChangeText={(text) => {
                                    setForm({
                                        ...form,
                                        name: {
                                            ...form.name,
                                            value: text,
                                        },
                                    });
                                }}
                            />
                        </View>
                        <View style={styles.inputContainer}>
                            <Text style={styles.textInput}>Sigla</Text>
                            <TextInput
                                style={styles.input}
                                placeholder={'Introduzca Sigla'}
                                placeholderTextColor="#888"
                                value={form.initial.value}
                                onChangeText={(text) => {
                                    setForm({
                                        ...form,
                                        initial: {
                                            ...form.initial,
                                            value: text,
                                        },
                                    });
                                }}
                            />
                        </View>
                        <View style={styles.inputContainer}>
                            <Text style={styles.textInput}>Docente</Text>
                            <DropDownPicker
                                autoScrollToDefaultValue={true}

                                placeholder="ninguno"
                                placeholderStyle={{ color: 'black' }}
                                items={
                                    data?.map((item) => ({
                                        label: item?.firstName+' '+item?.lastName,
                                        value: item?.id,
                                    })) ?? []
                                }
                                //defaultValue={select.tipo}
                                containerStyle={{ height: 40, width: "100%" }}
                                style={{ backgroundColor: Theme.COLORS.SECONDARY }}
                                itemStyle={{
                                    justifyContent: 'flex-start', width: '92%'
                                }}
                                dropDownStyle={{ backgroundColor: Theme.COLORS.SECONDARY }}
                                onChangeItem={data => {
                                    setForm({
                                        ...form,
                                        idTeacher: {
                                            ...form.idTeacher,
                                            value: data.value,                                                                                     
                                            //error: validateRequired(data.value),
                                        },teacher: {
                                            ...form.teacher,
                                            value: data.label,
                                        }
                                    })
                                    
                                }}
                            />
                        </View>


                    </View>
                    <View style={{ alignContent: 'center', position: 'absolute', bottom: 1, zIndex: -1 }}>
                        <Button
                            style={{ backgroundColor: Theme.COLORS.BUTTON }}
                            text={'Registrar'}
                            medium={true}
                            press={() => {
                                registry(form)
                            }}
                            disabled={false}
                        />
                    </View>

                    {/* <Button style={{ backgroundColor: '#53D769', position: 'absolute', bottom: '5%' }} text={'Presioname!!!!'} onPress={action}></Button> */}
                </View>

            </View>



        </View>

    )
}

const styles = StyleSheet.create({
    stretch: {
        width: 100,
        height: 100,
        borderRadius: 200,
    },
    container: {
        backgroundColor: Theme.COLORS.LIGHTGREY,
        flex: 1,
        width: '100%',
        height: '100%',

        alignItems: 'center',
        justifyContent: 'center',
    },
    containerTop: {
        backgroundColor: Theme.COLORS.PRIMARY,
        backgroundColor: "#049FFF",
        paddingVertical: 20,
        elevation: 5,
        position: 'absolute',
        width: "80%",
        left: "10%",
        height: '12%',
    },
    input: {
        marginVertical: 10,
        paddingLeft: 15,
        height: 45,
        borderRadius: 10,
        backgroundColor: 'rgba(72, 163, 198, 0.2)',
    },
    inputContainer: {
        width: '90%',
        //alignItems:'center',
        marginTop: 10,
        marginLeft: '5%'
    },

    inputText: {
        alignSelf: "center",
        width: '90%',
        height: 35,
        backgroundColor: Theme.COLORS.BACKGROUNDCOLOR,
        borderRadius: 12,
        fontSize: Theme.FONT.SMALL
    },
    button: {
        marginTop: 20,
        justifyContent: "center",
        alignItems: "center",
        width: '50%',
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        height: 30,
    },


    textInput: {
        color: Theme.COLORS.PRIMARY
    },

})
