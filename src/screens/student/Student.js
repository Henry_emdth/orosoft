
import React, { useEffect, useRef, useState, useMemo } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    Image,
    Alert,
    Pressable,
    ScrollView,
    FlatList,
    SafeAreaView
} from 'react-native'
import Theme from '../../constants/Theme'
import Button from '../../components/Button'
import Input from '../../components/Input';
import HeaderHome from '../../components/HeaderHome'
import firestore from '@react-native-firebase/firestore';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Icon, theme } from 'galio-framework';
import auth from '@react-native-firebase/auth';
import * as Animatable from 'react-native-animatable';


export default function Student({ navigation }) {
    const [loged, setLoged] = useState();
    const [type, setType] = useState(true);
    const [data, setData] = useState();
    const [form, setForm] = useState({
        firstName: { value: 'Ivan', required: true, error: true },
        lastName: { value: 'Escalante', required: true, error: true },
        email: { value: 'ivan@test.com', required: true, error: true },
        phone: { value: '76543210', required: true, error: true },
        address: { value: 'Z/Portada C/Ala Nro:1311', required: true, error: true },
        ci: { value: '8871242', required: true, error: true },
    });
    // const postStudent = (data) => {
    //     firestore()
    //         .collection('students')
    //         .doc(data.ci.value)
    //         .set({
    //             firstName: data.firstName.value,
    //             lastName: data.lastName.value,
    //             email: data.email.value,
    //             phone: data.phone.value,
    //             address: data.address.value,
    //             ci: data.ci.value,
    //         })
    //         .then(() => {
    //             navigation.navigate('Home');
    //         }
    //         )
    //         .catch((error) => {
    //             console.log(
    //                 'Something went wrong with added user to firestore: ',
    //                 error,
    //             );
    //         });
    // }
    //nsole.log('===---')

    useEffect(() => {
        auth().onAuthStateChanged(function (user) {
            if (user) {
                setLoged(user.uid)
                //console.log(user, '===')
                firestore()
                    .collection('students')
                    .doc(loged)
                    .onSnapshot(querySnapshot => {
                        if (querySnapshot.data()) {
                            setData(querySnapshot.data());
                        }
                    })
                setType(true);
            }
        });
    }, [loged]);
    console.log(data, '===T data')

    return (
        <>
            <HeaderHome
                navigation={navigation}
                color={Theme.COLORS.BACKGROUNDCOLOR}
                title={"MI PERFIL"}
            />
           
       
            <Animatable.View style={styles.container}
             animation={'fadeInUp'}
        duration={2000}
            >
                <View>
                    <Image
                        style={styles.stretch}
                        source={require('../../assets/profile.png')}
                    //source={{ uri: 'https://reactnative.dev/img/tiny_logo.png' }}
                    />
                </View>
                <View style={styles.infoContainer}>
                    <Text style={styles.description}>Datos personales</Text>
                    <View >
                        <Text>Nombre</Text>
                        <Text style={styles.info}>{data?.firstName}</Text>
                    </View>
                    <View>
                        <Text>Apellido</Text>
                        <Text style={styles.info}>{data?.lastName}</Text>
                    </View>
                    <View>
                        <Text>Cedula de Identidad</Text>
                        <Text style={styles.info}>{data?.ci}</Text>
                    </View>
                    <View>
                        <Text>Correo Electronico: </Text>
                        <Text style={styles.info}>{data?.email}</Text>
                    </View>
                    <View>
                        <Text>Numero telefonico: </Text>
                        <Text style={styles.info}>{data?.phone}</Text>
                    </View>

                </View>
                <Button
                    style={{ backgroundColor: Theme.COLORS.BUTTON }}
                    text={'ACTUALIZAR DATOS'}
                    medium={true}
                    press={() => {
                        //postStudent(form)

                    }}
                    disabled={false}
                />
            </Animatable.View>
        </>
    )
}

const styles = StyleSheet.create({
    stretch: {
        width: 100,
        height: 100,
        borderRadius: 200,
        borderWidth: 4,
        borderColor: Theme.COLORS.PRIMARY,
        marginVertical: 5,
    },
    container: {
        backgroundColor: Theme.COLORS.LIGHTGREY,
        flex: 1,
        alignItems: 'center',
        //justifyContent: 'center',
    },

    description: {
        backgroundColor: Theme.COLORS.GRADIENT_START,
        textAlign: 'center',
        color: Theme.COLORS.LIGHTGREY,
        height: 30,
        fontSize: 20,
        borderRadius: 10,
    },
    info: {
        borderWidth: 2,
        height: 30,
        textAlignVertical: 'center',
        paddingLeft: 20,
        borderRadius: 10,
        borderColor: Theme.COLORS.PRIMARY,
        backgroundColor: Theme.COLORS.SECONDARY,
        color: Theme.COLORS.DARKGREY,
        marginBottom: 10,
    },
    input: {
        marginVertical: 10,
        paddingLeft: 15,
        height: 45,
        borderRadius: 10,
        backgroundColor: 'rgba(72, 163, 198, 0.2)',
    },
    infoContainer: {
        width: '90%'
    },
    inputContainer: {
        width: '90%',
        //alignItems:'center',
        marginTop: 10,
        marginLeft: '5%'
    },

    inputText: {
        alignSelf: "center",
        width: '90%',
        height: 35,
        backgroundColor: Theme.COLORS.BACKGROUNDCOLOR,
        borderRadius: 12,
        fontSize: Theme.FONT.SMALL
    },
    button: {
        marginTop: 20,
        justifyContent: "center",
        alignItems: "center",
        width: '50%',
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        height: 30,
    },
    iconsMid: {
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    containerTopRed: {
        backgroundColor: '#C64848',
        height: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    positionTopRed: {
        width: '90%',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    textInput: {
        color: Theme.COLORS.PRIMARY
    },
    buttonTop: {
        backgroundColor: Theme.COLORS.BACKGROUNDCOLOR,
        borderRadius: 10,
        width: 60,
        alignItems: 'center',
    },
})
