
import React, { useEffect, useRef, useState, useMemo } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    Image,
    Alert,
    Pressable,
    ScrollView,
    FlatList,
    SafeAreaView
} from 'react-native'
import Theme from '../../constants/Theme'
import Button from '../../components/Button'
import Input from '../../components/Input';
import HeaderHome from '../../components/HeaderHome'
import firestore from '@react-native-firebase/firestore';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Icon } from 'galio-framework';
import auth from '@react-native-firebase/auth';


export default function Historial({ navigation }) {
    const [loged, setLoged] = useState();
    const [type, setType] = useState(true);
    const [data, setData] = useState();
    const [form, setForm] = useState({
        firstName: { value: 'Ivan', required: true, error: true },
        lastName: { value: 'Escalante', required: true, error: true },
        email: { value: 'ivan@test.com', required: true, error: true },
        phone: { value: '76543210', required: true, error: true },
        address: { value: 'Z/Portada C/Ala Nro:1311', required: true, error: true },
        ci: { value: '8871242', required: true, error: true },
    });
    useEffect(() => {
        auth().onAuthStateChanged(function (user) {
            if (user) {
                setLoged(user.uid)
                //console.log(user, '===')
                firestore()
                    .collection('students')
                    .doc(loged)
                    .onSnapshot(querySnapshot => {
                        if (querySnapshot.data()) {
                            setData(querySnapshot.data());
                        }
                    }),
                    
                setType(true);
            }
        });
    }, [loged]);
    console.log(data,'-=-')
    return (
        <>
            <HeaderHome
                navigation={navigation}
                color={Theme.COLORS.BACKGROUNDCOLOR}
                title={"HISTORIAL ACADEMICO"}
            />
            <View style={styles.container}>
                <View style={styles.profile}>
                    <Image
                        style={styles.stretch}
                        source={require('../../assets/profile.png')}
                    //source={{ uri: 'https://reactnative.dev/img/tiny_logo.png' }}
                    />
                    <View style={styles.textInfo}>
                        <Text>
                            - Estudiente {data?.lastName} {data?.firstName} con cedula de identidad No. {data?.ci} y correo electronico {data?.email}.
                        </Text>
                    </View>
                </View>
                <View style={[styles.textDescription, { width: '90%', backgroundColor: Theme.COLORS.SUCCESS, padding: 5 }]}>
                    <Text style={styles.description}>Sigla</Text>
                    <Text style={styles.description}>Materia</Text>
                    <Text style={styles.description}>Nota</Text>
                    <Text style={styles.description}>observacion</Text>
                </View>
                <FlatList style={{ width: '90%' }}
                    data={data?.historial}
                    renderItem={
                        ({ item }) => (<View style={[styles.textDescription, { marginVertical: 5 }]}>
                            <Text>{item.sigla}</Text>
                            <Text>{item.nombre}</Text>
                            <Text>{item.nota}</Text>
                            <Text>{item.observacion}</Text>
                        </View>
                        )}
                //keyExtractor={item => item.id}
                />
                <View style={styles.containerResult}>                   
                    <Text style={{ color: Theme.COLORS.LIGHTGREY }}>INSCRIPTAS: </Text>
                    <Text style={{ color: Theme.COLORS.SUCCESS }}>{ }0</Text>
                    <Text style={{ color: Theme.COLORS.LIGHTGREY }}>REPROBADAS: </Text>
                    <Text style={{ color: Theme.COLORS.WARNING }}>{ }0</Text>
                </View>
                <View style={styles.containerResult}>                   
                    <Text style={{ color: Theme.COLORS.LIGHTGREY }}>APROBADAS: </Text>
                    <Text style={{ color: Theme.COLORS.SUCCESS }}>{ }0</Text>
                    <Text style={{ color: Theme.COLORS.LIGHTGREY }}>PROM. APROBADAS: </Text>
                    <Text style={{ color: Theme.COLORS.PRIMARY }}>{ }0</Text>
                </View>
            </View>
        </>


    )
}
const styles = StyleSheet.create({
    stretch: {
        width: '20%',
        height: 50,
        marginVertical: 10,
        borderRadius: 200,
    },
    container: {
        backgroundColor: Theme.COLORS.LIGHTGREY,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    containerResult: {
        flexDirection: 'row',
        width: '90%',
        padding: 20,
        justifyContent: 'space-between',
        backgroundColor: Theme.COLORS.GRADIENT_START,
        //marginBottom: 10,
        //borderRadius: 15,
    },
    containerTop: {
        backgroundColor: Theme.COLORS.PRIMARY,
        backgroundColor: "#049FFF",
        paddingVertical: 20,
        elevation: 5,
        position: 'absolute',
        width: "80%",
        left: "10%",
        height: '12%',
    },
    input: {
        marginVertical: 10,
        paddingLeft: 15,
        height: 45,
        borderRadius: 10,
        backgroundColor: 'rgba(72, 163, 198, 0.2)',
    },
    inputContainer: {
        width: '90%',
        //alignItems:'center',
        marginTop: 10,
        marginLeft: '5%'
    },

    inputText: {
        alignSelf: "center",
        width: '90%',
        height: 35,
        backgroundColor: Theme.COLORS.BACKGROUNDCOLOR,
        borderRadius: 12,
        fontSize: Theme.FONT.SMALL
    },
    button: {
        marginTop: 20,
        justifyContent: "center",
        alignItems: "center",
        width: '50%',
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        height: 30,
    },
    iconsMid: {
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    containerTopRed: {
        backgroundColor: '#C64848',
        height: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    description: {
        color: Theme.COLORS.LIGHTGREY,
    },
    positionTopRed: {
        width: '90%',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    textInput: {
        color: Theme.COLORS.PRIMARY
    },
    buttonTop: {
        backgroundColor: Theme.COLORS.BACKGROUNDCOLOR,
        borderRadius: 10,
        width: 60,
        alignItems: 'center',
    },
    profile: {
        flexDirection: 'row',
        width: '90%',
        marginVertical: 10,
    },
    textDescription: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    textInfo: {
        width: '80%',
        padding: 10,
        backgroundColor: Theme.COLORS.SECONDARY,
        borderRadius: 10,
    },
})
