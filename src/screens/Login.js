import React, { useState, useRef, useEffect } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  Animated,
  Alert,
} from 'react-native';
import Button from '../components/Button';
import Input from '../components/Input';
import Theme from '../constants/Theme';
import auth from '@react-native-firebase/auth';
import * as Animatable from 'react-native-animatable';
export default function Login({ navigation }) {

  const fadeAnim = useRef(new Animated.Value(0)).current;
  const [user, setUSer] = useState({ email: 'user3@test.com', password: 'test1235' });
  const [loged, setLoged] = useState();

  const fadeIn = () => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 500,
    }).start();
  };
  const login = (credentials) => {
    auth()
      .signInWithEmailAndPassword(credentials.email, credentials.password)
      .then(() => {
        auth().onAuthStateChanged(function (user) {
        navigation.navigate(user.displayName==='students'?'Student':'Teacher');
        });
        console.log('Signed in!');        
      })
      .catch((error) => {
        if (error.code === 'auth/user-not-found') {
          Alert.alert('Error', 'El email no existe', [
            {
              text: 'Ok',
            },
          ]);
        }
        if (error.code === 'auth/wrong-password') {
          Alert.alert('Error', 'La contraseña es incorrecta', [
            {
              text: 'Ok',
            },
          ]);
        }
      });
  };
  useEffect(() => {
    fadeIn();
  });

  return (
    <View style={styles.container}>
      <Text style={styles.tittle}>BIENVENIDO</Text>

      <Animated.View
      
        elevation={10}
        style={[
          styles.formContainer,
          {
            opacity: fadeAnim,
          },
        ]}>
        <View style={styles.textContain}>
          <TouchableOpacity>
            <Text style={styles.textTab}>Iniciar Sesion</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() =>
            navigation.navigate('Signup')
          }>
            <Text style={[styles.textTab, { color: '#888' }]}>REGISTRO</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.inputContainer}>
          <Text>Usuario</Text>
          <TextInput
            style={styles.input}
            placeholder="Introduzca su Usuario"
            placeholderTextColor="#888"
            value={user.email}
            onChangeText={(value) => {
              setUSer({ ...user, email: value });
            }}
          />
        </View>
        <View style={styles.inputContainer}>
          <Text>Contraseña</Text>
          <TextInput
            style={styles.input}
            placeholder="Introduzca su Contraseña"
            placeholderTextColor="#888"
            secureTextEntry={true}
            value={user.password}
            onChangeText={(value) => {
              setUSer({ ...user, password: value });
            }}
          />
        </View>
        <Text style={styles.text}>Olvidaste tu contraseña</Text>
        <Button
          style={{ backgroundColor: Theme.COLORS.BUTTON }}
          text={'Iniciar Sesion'}
          medium={true}
          press={() => {
            login(user)

          }}
          disabled={false}
        />
      </Animated.View>
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
    flex: 1,
    backgroundColor: Theme.COLORS.PRIMARY,
    justifyContent: 'center',
    alignItems: 'center',
  },
  formContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 20,
    height: 400,
    width: '90%',
    backgroundColor: '#fff',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#bbb',
  },
  input: {
    marginVertical: 10,
    paddingLeft: 15,
    height: 45,
    borderRadius: 10,
    backgroundColor: 'rgba(72, 163, 198, 0.2)',
  },
  inputContainer: {
    width: '85%',
    marginTop: 10,
  },
  textTab: {
    fontSize: 20,
  },
  text: {
    marginVertical: 5,
    color: '#777',
  },
  textContain: {
    marginBottom: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: '80%',
  },
  tittle: {
    textAlign: 'center',
    fontFamily: 'Redressed',
    color: '#fff',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
    fontSize: 30,
  },
});
