
import React, { useEffect, useRef, useState, useMemo } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    Image,
    Alert,
    Pressable,
    ScrollView,
    FlatList,
    SafeAreaView
} from 'react-native'
import Theme from '../constants/Theme'
import Button from '../components/Button';
import Input from '../components/Input';
import HeaderHome from '../components/HeaderHome'
import firestore from '@react-native-firebase/firestore';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Icon } from 'galio-framework';


export default function Home({ navigation }) {
    const action = () => {
        Alert.alert(
            "DATOS PERSONALES",
            "Nombre: Henry Villalobos",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "OK", onPress: () => console.log("OK Pressed") }
            ],
            { cancelable: false }
        );
    }
    const [form, setForm] = useState({
        firstName: { value: 'Ivan', required: true, error: true },
        lastName: { value: 'Escalante', required: true, error: true },
        email: { value: 'ivan@test.com', required: true, error: true },
        phone: { value: '76543210', required: true, error: true },
        address: { value: 'Z/Portada C/Ala Nro:1311', required: true, error: true },
        ci: { value: '8871242', required: true, error: true },
    });
    const postStudent = (data) => {
        firestore()
            .collection('students')
            .doc(data.ci.value)
            .set({
                firstName: data.firstName.value,
                lastName: data.lastName.value,
                email: data.email.value,
                phone: data.phone.value,
                address: data.address.value,
                ci: data.ci.value,
            })
            .then(() => {
                navigation.navigate('Home');
            }
            )
            .catch((error) => {
                console.log(
                    'Something went wrong with added user to firestore: ',
                    error,
                );
            });
    }
    function RegisterScreen() {
        return (
            <View style={styles.container}>
                
                {/* <View style={{ width: '88%', height: '90%', backgroundColor: '#fff', borderRadius: 20, elevation: 7, alignItems: 'center', paddingTop: 20 }}> */}
                <Image style={{ marginBottom: '10%', width: 100, height: 100, borderWidth: 5, borderColor: '#ddd', borderRadius: 200 }}
                    source={require('../assets/profile.png')} />
                <Text>Universidad Mayor de San Andres</Text>

                <ScrollView style={{ width: '100%' }}>
                    <View style={styles.inputContainer}>
                        <Text style={styles.textInput}>Nombre</Text>
                        <TextInput
                            style={styles.input}
                            placeholder={'Introduzca nombre'}
                            placeholderTextColor="#888"
                            value={form.firstName.value}
                            onChangeText={(text) => {
                                setForm({
                                    ...form,
                                    firstName: {
                                        ...form.firstName,
                                        value: text,
                                    },
                                });
                            }}
                        />
                    </View>
                    <View style={styles.inputContainer}>
                        <Text style={styles.textInput}>Apellido</Text>
                        <TextInput
                            style={styles.input}
                            placeholder={'Introduzca apellido'}
                            placeholderTextColor="#888"
                            value={form.lastName.value}
                            onChangeText={(text) => {
                                setForm({
                                    ...form,
                                    lastName: {
                                        ...form.lastName,
                                        value: text,
                                    },
                                });
                            }}
                        />
                    </View>
                    <View style={styles.inputContainer}>
                        <Text style={styles.textInput}>Cedula de identidad</Text>
                        <TextInput
                            style={styles.input}
                            placeholder={'Introduzca C.I.'}
                            placeholderTextColor="#888"
                            value={form.ci.value}
                            onChangeText={(text) => {
                                setForm({
                                    ...form,
                                    ci: {
                                        ...form.ci,
                                        value: text,
                                    },
                                });
                            }}
                        />
                    </View>
                    <View style={styles.inputContainer}>
                        <Text style={styles.textInput}>Correo electronico</Text>
                        <TextInput
                            style={styles.input}
                            placeholder={'Introduzca correo'}
                            placeholderTextColor="#888"
                            value={form.email.value}
                            onChangeText={(text) => {
                                setForm({
                                    ...form,
                                    email: {
                                        ...form.email,
                                        value: text,
                                    },
                                });
                            }}
                        />
                    </View>
                    <View style={styles.inputContainer}>
                        <Text style={styles.textInput}>Direccion</Text>
                        <TextInput
                            style={styles.input}
                            placeholder={'Introduzca direccion'}
                            placeholderTextColor="#888"
                            value={form.address.value}
                            onChangeText={(text) => {
                                setForm({
                                    ...form,
                                    address: {
                                        ...form.address,
                                        value: text,
                                    },
                                });
                            }}
                        />
                    </View>
                    <View style={{ alignItems: 'center' }}>
                        <Button
                            style={{ backgroundColor: Theme.COLORS.BUTTON }}
                            text={'Registrar'}
                            medium={true}
                            press={() => {
                                postStudent(form)

                            }}
                            disabled={false}
                        />

                    </View>
                </ScrollView>
                {/* <Button style={{ backgroundColor: '#53D769', position: 'absolute', bottom: '5%' }} text={'Presioname!!!!'} onPress={action}></Button> */}
                {/* </View> */}

            </View>
        );
    };


    useEffect(() => {
        const subscriber = firestore()
          .collection('students')
          .onSnapshot(querySnapshot => {
            const users = [];
      
            querySnapshot.forEach(documentSnapshot => {
              users.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setData(users);
          });
      
        // Unsubscribe from events when no longer in use
        return () => subscriber();
      }, []);
    const [data, setData] = useState([]);
    const ListScreen =()=>{
       console.log({data})
        return (
            <SafeAreaView style={styles.container}>
                <Text style={{fontSize:20}}>LISTA DE ESTUDIANTES REGISTRADOS</Text>
                <FlatList style={{width:'100%'}}
                    data={data}
                    renderItem={
                        ({ item }) =>(
                            <View style={{backgroundColor:Theme.COLORS.BACKGROUNDCOLOR,margin:'2%',padding:'2%'}}>
                                <Text>{item.firstName}</Text>
                                <Text>{item.lastName}</Text>
                                <Text>{item.ci}</Text>
                                <Text>{item.email}</Text>
                            </View>
                        )

                    }
                    //keyExtractor={item => item.id}
                />
            </SafeAreaView>
        );
    }
    const Tab = createBottomTabNavigator();
    //abrir la aplicaion insertar grafico Boton, presionan el Boton mostra mensaje
    return (
        <>
            <HeaderHome
                navigation={navigation}
                color={Theme.COLORS.BACKGROUNDCOLOR}
                title={"REGISTRO DE ESTUDIANTES"}
            />
            {/* <View style={styles.containerTop}>
                <TextInput
                    style={styles.inputText}
                    value={""}
                    placeholder="Buscar"
                    returnKeyType={'search'}
                    onChangeText={
                        (text) => {
                            //setInputTextSearch(text)
                        }
                    }
                />
            </View> */}
            <NavigationContainer independent={true}>
                <Tab.Navigator
                    screenOptions={({ route }) => ({
                        tabBarIcon: ({ focused, color, size }) => {
                            let iconName;

                            if (route.name === 'Registro') {
                                iconName = focused ? 'ios-information-circle' : 'ios-information-circle-outline';
                            } else if (route.name === 'Listado') {
                                iconName = focused ? 'ios-list-box' : 'ios-list';
                            }

                            // You can return any component that you like here!
                            return <Icon
                                name={'app-registration'}
                                family={"MaterialIcons"}
                                color={"red"}
                                size={30}
                            />;
                        },
                    })}
                    tabBarOptions={{
                        activeTintColor: 'tomato',
                        inactiveTintColor: 'gray',
                    }}
                >
                    <Tab.Screen name="Registro" component={RegisterScreen} />
                    <Tab.Screen name="Listado" component={ListScreen} />
                </Tab.Navigator>
            </NavigationContainer>
        </>
        // <View style={{backgroundColor:'red'}}>
        //     <Animatable.View
        //         animation={"fadeInDown"}
        //         style={styles.containerTop}
        //     >
        //         <HeaderHome
        //             navigation={navigation}
        //             color={Theme.COLORS.BACKGROUNDCOLOR}
        //             title={"App"}
        //         />
        //         <TextInput
        //             style={styles.inputText}
        //             value={inputTextSearch}
        //             placeholder="Buscar"
        //             returnKeyType={'search'}
        //             //onSubmitEditing={()=>setInputSearch(inputTextSearch),console.log(inputTextSearch)}
        //             onChangeText={
        //                 (text) => {
        //                     //setInputTextSearch(text)
        //                 }
        //             }
        //         />

        //         {/* <View style={{ flexDirection: "row" }}>
        //             <TouchableOpacity
        //                 style={[styles.button, { backgroundColor: selectButton ? Theme.COLORS.BACKGROUNDCOLOR : Theme.COLORS.PRIMARY }]}
        //                 onPress={() => {
        //                     setSelectButton(true)
        //                     //setSelectMap(false)
        //                 }}
        //             >
        //                 <Text style={{ fontSize: Theme.FONT.SMALL, color: selectButton ? '#000000' : Theme.COLORS.BACKGROUNDCOLOR }}>Perdidos</Text>
        //             </TouchableOpacity>
        //             <TouchableOpacity
        //                 style={[styles.button, { backgroundColor: !selectButton ? Theme.COLORS.BACKGROUNDCOLOR : Theme.COLORS.PRIMARY }]}
        //                 onPress={() => {
        //                     setSelectButton(false)
        //                     //setSelectMap(false)
        //                 }}
        //             >
        //                 <Text style={{ fontSize: Theme.FONT.SMALL, color: !selectButton ? '#000000' : Theme.COLORS.BACKGROUNDCOLOR }}>Encontrados</Text>
        //             </TouchableOpacity>
        //         </View> */}
        //     </Animatable.View>
        //     <Text>HOLA</Text>
        //     <View>
        //         <Image
        //             style={styles.stretch}
        //             source={require('../assets/profile.png')}
        //             //source={{ uri: 'https://reactnative.dev/img/tiny_logo.png' }}
        //         />
        //     </View>
        //     <Text>sd</Text>
        // </View>
    )
}

const styles = StyleSheet.create({
    stretch: {
        width: 100,
        height: 100,
        borderRadius: 200,
    },
    container: {
        backgroundColor: Theme.COLORS.LIGHTGREY,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    containerTop: {
        backgroundColor: Theme.COLORS.PRIMARY,
        backgroundColor: "#049FFF",
        paddingVertical: 20,
        elevation: 5,
        position: 'absolute',
        width: "80%",
        left: "10%",
        height: '12%',
    },
    input: {
        marginVertical: 10,
        paddingLeft: 15,
        height: 45,
        borderRadius: 10,
        backgroundColor: 'rgba(72, 163, 198, 0.2)',
    },
    inputContainer: {
        width: '90%',
        //alignItems:'center',
        marginTop: 10,
        marginLeft: '5%'
    },

    inputText: {
        alignSelf: "center",
        width: '90%',
        height: 35,
        backgroundColor: Theme.COLORS.BACKGROUNDCOLOR,
        borderRadius: 12,
        fontSize: Theme.FONT.SMALL
    },
    button: {
        marginTop: 20,
        justifyContent: "center",
        alignItems: "center",
        width: '50%',
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        height: 30,
    },
    iconsMid: {
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    containerTopRed: {
        backgroundColor: '#C64848',
        height: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    positionTopRed: {
        width: '90%',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    textInput: {
        color: Theme.COLORS.PRIMARY
    },
    buttonTop: {
        backgroundColor: Theme.COLORS.BACKGROUNDCOLOR,
        borderRadius: 10,
        width: 60,
        alignItems: 'center',
    },
})
