import React, { useEffect, useState, useRef } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    ImageBackground,
    TextInput,
    ScrollView,
    TouchableOpacity,
    Alert,
} from 'react-native';

import Button from '../components/Button';
import Theme from '../constants/Theme';
import { Icon } from 'galio-framework';
import * as Animatable from 'react-native-animatable';
import firestore from '@react-native-firebase/firestore';
import auth, { firebase } from '@react-native-firebase/auth';

import {
    validateRequired,
    validateEmail,
    validatePassword,
    validateForm,
    parseForm,
    validateNumber,
} from '../utils/authValidation';
import DropDownPicker from 'react-native-dropdown-picker';
import { color } from 'react-native-reanimated';

export default function Update({ navigation }) {
    const [form, setForm] = useState({
        firstName: { value: '', required: true, error: false },
        lastName: { value: '', required: true, error: false },
        user: { value: 'user@test.com', required: false, error: false },
        ci: { value: '87878783', required: true, error: false },
        type: { value: '', required: true, error: false },
        phone: { value: '22222222', required: true, error: false },
        password: { value: 'test1235', required: true, error: false },
        rePassword: { value: 'test1235', required: true, error: false },
    });
    const [loged,setLoged]=useState()

    const tipos = [
        { answer: 'ESTUDIANTE', label: "students" },
        { answer: 'DOCENTE', label: "docents" },

    ]
    const [fetching, setFetching] = useState(false);
    const [aceptTermins, setAceptTermins] = useState(false);

    const signupUser = (userDetails) => {

        //dispatch(signup(userDetails))
        const { firstName, lastName, user, ci, phone, type, password } = userDetails;
        auth()
            .createUserWithEmailAndPassword(user.value, password.value)
            .then(() => {
                firestore()
                    .collection(type.value === 'students' ? 'students' : 'teachers')
                    .doc(auth().currentUser.uid)
                    .set({
                        firstName: firstName.value,
                        lastName: lastName.value,
                        email: user.value,
                        ci: ci.value,
                        phone: phone.value,
                    })
                    .then(() => {
                        login(form)
                        //navigation.navigate('Contacts', { type: 'register' });
                    }
                    )
                    .catch((error) => {
                        console.log(
                            'Something went wrong with added user to firestore: ',
                            error,
                        );
                    });

            })
            .catch((error) => {
                if (error.code === 'auth/email-already-in-use') {
                    Alert.alert('Error', 'El usuario ya existe', [
                        {
                            text: 'Ok',
                        },
                    ]);
                }
                if (error.code === 'auth/invalid-email') {
                    Alert.alert('Error', 'El usuario no es valido', [
                        {
                            text: 'Ok',
                        },
                    ]);
                }
                console.error(error);
            });
        const use = firebase.auth().currentUser
        use?.updateProfile({ displayName: type.value })

    };
    const login = (credentials) => {
        auth()
            .signInWithEmailAndPassword(credentials.user.value, credentials.password.value)
            .then(() => {
                console.log('Signed in!');
                navigation.navigate('Student');
            })
            .catch((error) => {
                if (error.code === 'auth/user-not-found') {
                    Alert.alert('Error', 'El email no existe', [
                        {
                            text: 'Ok',
                        },
                    ]);
                }
                if (error.code === 'auth/wrong-password') {
                    Alert.alert('Error', 'La contraseña es incorrecta', [
                        {
                            text: 'Ok',
                        },
                    ]);
                }
            });
    };
    useEffect(() => {
        auth().onAuthStateChanged(function (user) {
            if (user) {
                setLoged(user.uid)
                //console.log(user, '===')
                firestore()
                    .collection('students')
                    .doc(loged)
                    .onSnapshot(querySnapshot => {
                        if (querySnapshot.data()) {
                            setData(querySnapshot.data());
                        }
                    })
               
            }
        });
    }, [loged]);


    console.log(form)
    return (

        <ScrollView style={{ backgroundColor: Theme.COLORS.PRIMARY }}>
            <Animatable.Text
                animation={'bounceIn'}
                duration={2000}
                style={styles.title}>
                ACTUALIZACION DE DATOS
        </Animatable.Text>
            <Animatable.View
                animation={'bounceInUp'}
                duration={2000}
                style={[styles.formRegister]}>
                {/* <Text style={styles.titleRegister}>Registrarme</Text> */}
                <View style={styles.textContain}>
                    <Text style={[styles.titleRegister, { color: '#888' }]}>
                        Iniciar Sesion
              </Text>
                </View>
                <Text style={styles.titleInput}>Nombre</Text>
                <TextInput
                    style={styles.textInput}
                    value={form.firstName.value}
                    enablesReturnKeyAutomatically
                    placeholder="Nombre de usuario"
                    //clearButtonMode={'always'}
                    //label={'Nombres *'}
                    onChangeText={(text) => {
                        setForm({
                            ...form,
                            firstName: {
                                ...form.firstName,
                                value: text,
                                error: validateRequired(text),
                            },
                        });
                    }}
                    error={form.firstName.error}
                />
                <Text style={styles.titleInput}>Apellido</Text>
                <TextInput
                    style={styles.textInput}
                    value={form.lastName.value}
                    placeholder="Apellido de usuario"
                    //label={'Apellidos *'}
                    onChangeText={(text) => {
                        setForm({
                            ...form,
                            lastName: {
                                ...form.lastName,
                                value: text,
                                error: validateRequired(text),
                            },
                        });
                    }}
                    error={form.lastName.error}
                />
                <Text style={styles.titleInput}>Correo Electronico</Text>
                <TextInput
                    style={styles.textInput}
                    value={form.user.value}
                    placeholder="Usuario"
                    //label={'Usuario *'}
                    onChangeText={(text) => {
                        setForm({
                            ...form,
                            user: {
                                ...form.user,
                                value: text,
                                error: validateRequired(text),
                            },
                        });
                    }}
                    error={form.user.error}
                />
                <Text style={styles.titleInput}>Cedula de Identidad</Text>
                <TextInput
                    style={styles.textInput}
                    value={form.ci.value}
                    placeholder="Cedula"
                    //label={'Usuario *'}
                    onChangeText={(text) => {
                        setForm({
                            ...form,
                            ci: {
                                ...form.ci,
                                value: text,
                                error: validateRequired(text),
                            },
                        });
                    }}
                    error={form.user.error}
                />
                <Text style={styles.titleInput}>Tipo</Text>
                <DropDownPicker
                    placeholder="ninguno"
                    placeholderStyle={{ color: 'black' }}
                    items={
                        tipos?.map((item) => ({
                            label: item.answer,
                            value: item.label,
                        })) ?? []
                    }
                    //defaultValue={select.tipo}
                    containerStyle={{ height: 40, width: "92%" }}
                    style={{ backgroundColor: Theme.COLORS.SECONDARY }}
                    itemStyle={{
                        justifyContent: 'flex-start', width: '92%'
                    }}
                    dropDownStyle={{ backgroundColor: Theme.COLORS.SECONDARY }}
                    onChangeItem={data => {
                        setForm({
                            ...form,
                            type: {
                                ...form.type,
                                value: data.value,
                                error: validateRequired(data.value),
                            },
                        });
                    }}
                />
                <Text style={styles.titleInput}>Telefono</Text>
                <TextInput
                    keyboardType={'numeric'}
                    placeholder="Numero telefonico"
                    style={styles.textInput}
                    value={form.phone.value}
                    //label={'Usuario *'}
                    onChangeText={(text) => {
                        setForm({
                            ...form,
                            phone: {
                                ...form.phone,
                                value: text,
                                error: validateRequired(text),
                            },
                        });
                    }}
                    error={form.phone.error}
                />
                <Text style={styles.titleInput}>Password</Text>
                <TextInput
                    style={styles.textInput}
                    placeholder="Introduzca Password"
                    value={form.password.value}
                    secureTextEntry={true}
                    onChangeText={(text) => {
                        setForm({
                            ...form,
                            password: {
                                ...form.password,
                                value: text,
                                error: validateRequired(text) || validatePassword(text, text),
                            },
                        });
                    }}
                    error={form.password.error}
                />
                <Text style={styles.titleInput}>Repetir Password</Text>
                <TextInput
                    style={styles.textInput}
                    placeholder="Repita Password"
                    value={form.rePassword.value}
                    secureTextEntry={true}
                    onChangeText={(text) => {
                        setForm({
                            ...form,
                            rePassword: {
                                ...form.rePassword,
                                value: text,
                                error:
                                    validateRequired(text) ||
                                    validatePassword(form.password.value, text),
                            },
                        });
                    }}
                    error={form.rePassword.error}
                    viewPass
                    help="Debe tener al menos 8 caracteres"
                    bottomHelp
                />
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <Icon
                        name={aceptTermins ? 'check-box' : 'check-box-outline-blank'}
                        family="MaterialIcons"
                        size={20}
                        color={aceptTermins ? Theme.COLORS.PRIMARY : 'black'}
                        onPress={() => {
                            setAceptTermins(!aceptTermins);
                        }}></Icon>

                    <Text
                        style={{ color: aceptTermins ? Theme.COLORS.PRIMARY : 'black' }}>
                        Acepto términos y condiciones.
            </Text>
                </View>
                <View style={{ paddingRight: '5%' }}>
                    <Button
                        style={{ backgroundColor: Theme.COLORS.BUTTON }}
                        press={() => {
                            //console.log(form);
                            if (
                                !aceptTermins ||
                                form.firstName.error ||
                                form.lastName.error ||
                                form.user.error ||
                                form.phone.error ||
                                form.password.error ||
                                form.rePassword.error
                            ) {
                                Alert.alert(
                                    'Error',
                                    'El formulario contiene errores o algunos de los campos requeridos están vacíos. Por favor, revise.',
                                    [
                                        {
                                            text: 'Ok',
                                        },
                                    ],
                                );
                            } else {
                                signupUser(form);

                            }
                        }}
                        text={'Registrarme'}
                        large
                    />
                </View>
            </Animatable.View>
        </ScrollView>

    );
}
const styles = StyleSheet.create({
    title: {
        justifyContent: 'center',
        marginTop: 20,
        fontSize: 30,
        fontFamily: 'Redressed',
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10,
        color: '#FFFFFF',
        textAlign: 'center',

    },
    formRegister: {
        width: '90%',
        backgroundColor: '#FFFFFF',
        alignSelf: 'center',
        elevation: 10,
        borderRadius: 12,
        marginTop: 20,
        paddingLeft: 20,
    },
    titleRegister: {
        //marginTop: 10,
        fontSize: 20,
        textAlign: 'center',

    },
    textContain: {
        //marginBottom: 10,
        marginTop: 20,
        alignItems: 'center',
        //justifyContent: 'space-between',
        //flexDirection: 'row',
        width: '92%',
    },
    textInput: {
        width: '92%',
        marginVertical: 2,
        paddingLeft: 15,
        height: 45,
        borderRadius: 10,
        backgroundColor: 'rgba(72, 163, 198, 0.2)',
    },
    titleInput: {
        marginTop: 20,
        fontSize: 15,
    },
});
