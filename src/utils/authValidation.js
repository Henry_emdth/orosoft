export function validateForm(form) {
  let valid = true;
  for (let key in form) {
    if (form[key].required && validateRequired(form[key].value)) {
      valid = false;
      break;
    }
  }
  const password = validatePassword(
    form.password.value,
    form.rePassword ? form.rePassword.value : form.password.value,
  );
  if (!validateEmail(form.email.value) || password) {
    valid = false;
  }
  return valid;
}

export function validateEditForm(form) {
  let valid = true;
  for (let key in form) {
    if (form[key].required && validateRequired(form[key].value)) {
      form[key].value;
      valid = false;
      break;
    }
  }
  return valid;
}

export function validateRequired(item) {
  return item === '';
}

export function validateEmail(email) {
  return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);
}

export function validatePassword(password, rePassword) {
  return password.length < 8 || password !== rePassword;
}
